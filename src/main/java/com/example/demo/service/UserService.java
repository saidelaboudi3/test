package com.example.demo.service;

import com.example.demo.model.User;

import java.util.List;

public interface UserService {
    public User findUserById(Long id);
    public User findUserByEmail(String email);
    public List<User> findAllUsers();
    public User updateProdile(String email,User user);

}
